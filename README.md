# Sesame3 Bluetoothボタン+Raspberry Pi Zero W による制御

## 開発者
Toshimune Tanaka <t-toshi@j.email.ne.jp>

## 概要
* Sesame3デバイスを、Bluetoothボタンを使い、Sesame3 WebAPI経由で制御するためのシステム・プログラム。
* Raspberry Pi Zero W で動作。

## 前提環境
### 機器構成
| 必要機器              | 説明 |
| -------------------- | ----- |
| Raspberry Pi Zero W  | スイッチサイエンスで[ケースセット](https://www.switch-science.com/catalog/3201/)を購入 |
| CW Shutter           | 100均で330円で購入 |
| WiFi環境             | Raspberry Pi Zero W は有線LANなし。WiFiのみ。 |

### ソフトウェア(Raspberry Pi Zero W上)
* Rasbian OS
* Python3.7.3
  * evdev1-4.0
  * pysesame3-0.2.2
  * urllib3-1.26.4
* bluez
* bluetooth
* libbluetooth-dev
* build-essential
* bluez-cups

## 環境構築
* 予め、Raspberry Pi Zero WにOSをインストールし、SSH接続ができる状態にしておく。
* 以下は、SSH接続後の環境構築作業。

```bash
pi@raspberrypi:~ $ sudo apt-get update
pi@raspberrypi:~ $ sudo apt-get upgrade
pi@raspberrypi:~ $ sudo apt install bluez bluetooth libbluetooth-dev build-essential
pi@raspberrypi:~ $ sudo apt install bluez-cups
pi@raspberrypi:~ $ bluetoothctl

[bluetooth]# power on
Changing power on succeeded

[bluetooth]# scan on
Discovery started
[CHG] Controller B8:27:EB:86:66:F8 Discovering: yes
[NEW] Device B9:EA:8D:B9:EA:8E WM2
[NEW] Device FF:83:C7:8D:6C:CD 4QyAX3kddh0HVYpgBfJ1Vw
[NEW] Device 2A:07:98:10:31:BA CW Shutter
[CHG] Device 78:36:CC:B9:14:26 RSSI: -66

[bluetooth]# connect 2A:07:98:10:31:BA
Attempting to connect to 2A:07:98:10:31:BA
[CHG] Device 2A:07:98:10:31:BA Connected: yes
Connection successful
[NEW] Primary Service
        /org/bluez/hci0/dev_2A_07_98_10_31_BA/service0001
        00001801-0000-1000-8000-00805f9b34fb
        Generic Attribute Profile
[NEW] Characteristic
        /org/bluez/hci0/dev_2A_07_98_10_31_BA/service0001/char0002
        00002a05-0000-1000-8000-00805f9b34fb
        Service Changed
[NEW] Descriptor
        /org/bluez/hci0/dev_2A_07_98_10_31_BA/service0001/char0002/desc0004
        00002902-0000-1000-8000-00805f9b34fb
        Client Characteristic Configuration
[NEW] Primary Service
        /org/bluez/hci0/dev_2A_07_98_10_31_BA/service000c
        0000180a-0000-1000-8000-00805f9b34fb
        Device Information
[NEW] Characteristic
        /org/bluez/hci0/dev_2A_07_98_10_31_BA/service000c/char000d
        00002a50-0000-1000-8000-00805f9b34fb
        PnP ID
[CHG] Device 2A:07:98:10:31:BA UUIDs: 00001800-0000-1000-8000-00805f9b34fb
[CHG] Device 2A:07:98:10:31:BA UUIDs: 00001801-0000-1000-8000-00805f9b34fb
[CHG] Device 2A:07:98:10:31:BA UUIDs: 0000180a-0000-1000-8000-00805f9b34fb
[CHG] Device 2A:07:98:10:31:BA UUIDs: 0000180f-0000-1000-8000-00805f9b34fb
[CHG] Device 2A:07:98:10:31:BA UUIDs: 00001812-0000-1000-8000-00805f9b34fb
[CHG] Device 2A:07:98:10:31:BA ServicesResolved: yes

[CW Shutter]# pair 2A:07:98:10:31:BA
Attempting to pair with 2A:07:98:10:31:BA
Request authorization
[agent] Accept pairing (yes/no): yes
[CHG] Device 2A:07:98:10:31:BA Paired: yes
Pairing successful
[CHG] Device 2A:07:98:10:31:BA Modalias: usb:v248Ap8266d0001

[CW Shutter]# trust 2A:07:98:10:31:BA
[CHG] Device 2A:07:98:10:31:BA Trusted: yes
Changing 2A:07:98:10:31:BA trust succeeded

[CW Shutter]# quit


```

* ファイルの配備

```bassh
pi@raspberrypi:~ $ cd ~/
(デフォルトユーザーのディレクトリにプログラム用のディレクトリを作成)
pi@raspberrypi:~ $ mkdir opensesame3
(作成したディレクトリに、各種ファイルをコピー)
pi@raspberrypi:~ $ pip3 install -r requirements.txt
pi@raspberrypi:~ $ chmod 755 sesame3ctl.py
pi@raspberrypi:~ $ vi sesame3ctl.ini
(設定ファイルに必要なパラメーターを記載)
```

* 設定ファイル内容

```ini
[base]
# SECRET STRING Sesame device from QR code
# A string like the following, "ssm://UI?t=sk&sk=xxxx&l=0&n=%E7%8E%84%E9%96%A2%E3%81%AE%E9%8D%B5"
secret=[制御対象デバイスのQRコードから得られる文字列をセット]

# API-KEY from Sesame Web API
# https://dash.candyhouse.co/login
apikey=[Sesame WebAPI のAPI-KEYをセット]

# Your Sesame device UUID from Sesame3 App
uuid=[制御対象機器のUUIDをセット]
```

* systemdで自動起動

```bash
pi@raspberrypi:~ $ cd ~/
pi@raspberrypi:~ $ chmod 755 sesame3ctl.service
pi@raspberrypi:~ $ sudo cp sesame3ctl.service /etc/systemd/system/

pi@raspberrypi:~ $ sudo systemctl enable sesame3ctl.service
pi@raspberrypi:~ $ sudo systemctl start sesame3ctl.service
(確認は以下のコマンド)
pi@raspberrypi:~ $ sudo systemctl status sesame3ctl.service
```

## 動作
1. `CW Shutter`のボタンを押す（２つのうちどちらを押しても挙動は同じ）
2. １-２秒ほどで、Sesame3の鍵が動作する
    1. 動作はトグル型となるため、ロック状態時は解錠、解錠状態時はロックとの動作となる
3. `CW Shutter`の電源をOFF→ONしたときや、Bluetooth切断→接続のときは、ボタンを押してもすぐには反応しない（ボタンを押していればそのうち反応する）

## 制約事項
1. 制御できるSesame3デバイスは1台のみ
2. `CW Shutter`以外のBluetoothデバイスを使う場合、Bluetoothctlのペアリング時に対処し、キーイベントの帳尻を合わせる必要がある
