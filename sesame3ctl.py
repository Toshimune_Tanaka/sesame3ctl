#!/usr/bin/python

import evdev
import os
import sys
import urllib.parse
import base64
import logging
import logging.handlers
import configparser
from datetime import datetime as dt
from pysesame3.auth import WebAPIAuth
from pysesame3.lock import CHSesame2

datetimeformat = "%Y-%m-%d %H:%M:%S"

class sesamectlviaweb:

    def __init__(self, secret, apikey, uuid):
        self._secret = secret
        self._apikey = apikey
        self._uuid = uuid

    def control(self, remotename="None"):
        try:
            sk = urllib.parse.parse_qs(self._secret)
            sk_bytes = base64.b64decode(sk["sk"][0])
            secretkey = sk_bytes[1:17].hex()

            auth = WebAPIAuth(apikey=self._apikey)
            device = CHSesame2(authenticator=auth, device_uuid=self._uuid, secret_key=secretkey,)

            logging.info(dt.now().strftime(datetimeformat) + ": control [" + remotename + "]")
            res = device.toggle(history_tag="Remote key[" + remotename + "]")

            if True == res:
                return 1
            else:
                return -2
        except Exception as e:
            logging.info(dt.now().strftime(datetimeformat) + ": " + str(e))
            return -1


def main(sesamectlviaweb):
    try:
        sctl = sesamectlviaweb

        devices = [evdev.InputDevice(fn) for fn in evdev.list_devices()]
        for device in devices:
            print(device.fn, device.name, device.phys)

        eventfile = "/dev/input/event0"
        if False == os.path.exists(eventfile):
            return -2

        shutterdev = evdev.InputDevice(eventfile)
        print(shutterdev)

        for event in shutterdev.read_loop():
            if event.type == evdev.ecodes.EV_KEY:
                categorized = evdev.util.categorize(event)
                if 1 == categorized.keystate:
                    #logging.info(dt.now().strftime(datetimeformat) + ": " + categorized)
                    res = sctl.control("CWShutter")
                    if 1 == res:
                        logging.info(dt.now().strftime(datetimeformat) + ": Success control")
                    else:
                        logging.info(dt.now().strftime(datetimeformat) + ": Failed to control")

    except Exception as e:
        print(e)
        return -1


if __name__ == "__main__":
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter("[%(asctime)s]%(filename)s(%(lineno)d): %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    handler = logging.handlers.RotatingFileHandler(filename="sesame3ctl.log", maxBytes=1048576 , backupCount=5)
    formatter = logging.Formatter("[%(asctime)s]%(filename)s(%(lineno)d): %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    ini = configparser.ConfigParser(interpolation=None)
    ini.read(os.getcwd() + "/" + "sesame3ctl.ini", "UTF-8")

    secret = ini["base"]["secret"]
    apikey = ini["base"]["apikey"]
    uuid = ini["base"]["uuid"]

    logging.info(dt.now().strftime(datetimeformat) + ": secret-string:" + secret)
    logging.info(dt.now().strftime(datetimeformat) + ": api-key:" + apikey)
    logging.info(dt.now().strftime(datetimeformat) + ": uuid:" + uuid)

    sctl = sesamectlviaweb(secret, apikey, uuid)

    res = main(sctl)
    if 1 == res:
        logging.info(dt.now().strftime(datetimeformat) + ": Result [Success]")
    else:
        logging.info(dt.now().strftime(datetimeformat) + ": Result [Error]")
